import { Component, Input} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Farmer } from '../model/farmer.model';
import { FarmerService } from '../farmer/farmer.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-farmer',
  templateUrl: './farmer.component.html',
  styleUrls: ['./farmer.component.css'],
  providers: [FarmerService],
})

export class FarmerComponent {
  
  @Input()
  farmer: Farmer = new Farmer; 
  farmer$: Observable<Farmer>; 
  mensagem: string;
  erro: string;
  addressComplete: string;
  pesquisar: string;

  constructor(private farmerService: FarmerService,
    private route: ActivatedRoute, 
    private router: Router) { 
  }

  salvarFarmer(farmer: Farmer) {
    this.farmerService.salvarFarmer(farmer)
    .subscribe((data: Response) => {
       console.log('response status: '+data['code']);
       this.farmer = new Farmer();
       this.router.navigate(['/farmers'], { relativeTo: this.route });
      }, error => { this.erro = error}
   );
  }

  alterarFarmer(farmer: Farmer) {
    this.farmerService.alterarFarmer(farmer)
    .subscribe((data: Response) => {
      console.log('response status: '+data['code']);
       this.farmer = new Farmer();
       this.router.navigate(['/farmers'], { relativeTo: this.route });
    }, error => { this.erro = error}
   );    
  }

  deletarFarmer(farmer: Farmer) {
    this.farmerService.deletarFarmer(farmer)
    .subscribe((data: Response) => {
      console.log('response status: '+data['code']);
      this.farmer = new Farmer();
      this.router.navigate(['/farmers'], { relativeTo: this.route });
    }, error => { this.erro = error}
   );    
  }

  consultarFarmer(pesquisar: string){
    this.farmerService.consultarFarmer(pesquisar)
    .subscribe(response => {
      this.farmer = JSON.parse(response['clazz']);
    }, error => { this.erro = error}
   );      
  }

  voltar(){
    this.router.navigate(['/home'], { relativeTo: this.route });
  }

}