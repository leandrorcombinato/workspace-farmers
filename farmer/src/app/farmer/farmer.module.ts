import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FarmerComponent } from './farmer.component';
import { FarmerRoutingModule } from './farmer.routing.module';
import { FormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  declarations: [FarmerComponent],
  imports: [
    FormsModule,
    NgxMaskModule.forRoot(),
    CommonModule,
    FarmerRoutingModule    
  ]
})
export class FarmerModule { }