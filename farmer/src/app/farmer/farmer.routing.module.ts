import { NgModule } from '@angular/core';
import { Routes, RouterModule, ParamMap } from '@angular/router';
import { FarmerComponent } from './farmer.component';
import { switchMap } from 'rxjs/operators';

const routes: Routes = [
  { 
    path: '', 
    component: FarmerComponent
  }, 
  { 
    path: ':email', 
    component: FarmerComponent
  }    
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class FarmerRoutingModule { }
