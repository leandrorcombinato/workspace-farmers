import { Farmer } from '../model/farmer.model';

export declare abstract class FarmerSearchAbstractProvider {
  abstract searchFarmers(params: String): Promise<Farmer[]>;
}