import { Injectable, ValueProvider } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Observable, BehaviorSubject, throwError } from 'rxjs';
import { map, filter, catchError } from 'rxjs/operators';
import { Farmer } from '../model/farmer.model';
import { environment } from 'src/environments/environment';

declare var jQuery : any;
declare var $ : any;

@Injectable()
export class FarmerService {

    private _Url = environment.host + '/farmer';
  
    constructor(private http: HttpClient) { }

    salvarFarmer(farmer): Observable<Object>{
        let apiURL = this._Url+'/salvar'
        return this.http.post(apiURL, farmer)
          .pipe(
            catchError(this.handleError)
          );
    }

    alterarFarmer(farmer): Observable<Object>{
        let apiURL = this._Url+'/atualizar'
        return this.http.put(apiURL, farmer)
          .pipe(
            catchError(this.handleError)
          );
        
    }

    deletarFarmer(farmer): Observable<Object>{
        let apiURL = this._Url+'/deletar'
        return this.http.delete(apiURL, farmer)
          .pipe(
            catchError(this.handleError)
          );        
    }

    consultarFarmer(pesquisar): Observable<Farmer>{
        let apiURL = this._Url+'/search-farmer/'+pesquisar;
        return this.http.get<Farmer>(apiURL)
        .pipe(
            catchError(this.handleError)
        );   
    }

    private handleError(error: HttpErrorResponse) { 
        if (error.error instanceof ErrorEvent) {
          // A client-side or network error occurred. Handle it accordingly.
          console.error('An error occurred:', error.error.message);
        } else {
          // The backend returned an unsuccessful response code.
          // The response body may contain clues as to what went wrong,
          console.error('An error occurred:', error.error.message);
        }
        // return an observable with a user-facing error message
        return throwError(error.error.message);
    }    

}