import { Address } from './address.model';

export class Farmer {
    id: string;
    document: Document;
    name: string;
    address: Address;
}